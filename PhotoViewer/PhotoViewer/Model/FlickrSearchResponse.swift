//
//  FlickrSearchResponse.swift
//
//  Create on 10/3/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class FlickrSearchResponse: BaseModel {

    var photosResponse: PhotosResponse?
    var stat: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        photosResponse <- map["photos"]
        stat <- map["stat"]        
    }

}
