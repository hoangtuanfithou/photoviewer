//
//  Photo.swift
//
//  Create on 10/3/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class Photo: BaseModel {

    var farm: Int = 0
    var id: String = ""
    var isfamily: Int = 0
    var isfriend: Int = 0
    var ispublic: Int = 0
    var owner: String = ""
    var secret: String = ""
    var server: String = ""
    var title: String = ""
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        farm <- map["farm"]
        id <- map["id"]
        isfamily <- map["isfamily"]
        isfriend <- map["isfriend"]
        ispublic <- map["ispublic"]
        owner <- map["owner"]
        secret <- map["secret"]
        server <- map["server"]
        title <- map["title"]        
    }

    func flickrImageURL(_ size: String = "m") -> URL? {
        if let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_\(size).jpg") {
            return url
        }
        return nil
    }
}
