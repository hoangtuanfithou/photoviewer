//
//  FlickrSearchRequest.swift
//
//  Create on 10/3/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class FlickrSearchRequest: BaseModel {

    var apiKey: String = flickrApiKey
    var format: String = "json"
    var method: String = "flickr.photos.search"
    var nojsoncallback: Int = 1
    var perPage: Int?
    var text: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        apiKey <- map["api_key"]
        format <- map["format"]
        method <- map["method"]
        nojsoncallback <- map["nojsoncallback"]
        perPage <- map["per_page"]
        text <- map["text"]        
    }

}
