//
//  PhotosResponse.swift
//
//  Create on 10/3/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class PhotosResponse: BaseModel {

    var page: Int?
    var pages: Int?
    var perpage: Int?
    var photo: [Photo]?
    var total: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        page <- map["page"]
        pages <- map["pages"]
        perpage <- map["perpage"]
        photo <- map["photo"]
        total <- map["total"]        
    }

}
