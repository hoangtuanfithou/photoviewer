//
//  ViewController.swift
//  PhotoViewer
//
//  Created by Nguyen Hoang Tuan on 3/10/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage
import CHTCollectionViewWaterfallLayout
import SKPhotoBrowser
import SVProgressHUD

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CHTCollectionViewDelegateWaterfallLayout {

    var flickrSearchResponse = FlickrSearchResponse()
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBarSearchButtonClicked(searchBar)
        if let layout = (photosCollectionView.collectionViewLayout as? CHTCollectionViewWaterfallLayout) {
            layout.columnCount = 3
            layout.minimumColumnSpacing = 1
            layout.minimumInteritemSpacing = 1
        }
    }

    func searchFlickrPhoto(query: String) {
        let request = FlickrSearchRequest()
        request.text = query
        SVProgressHUD.show()
        Alamofire.request(flickrUrl, method: .get, parameters: request.toJSON()).responseObject { (response: DataResponse<FlickrSearchResponse>) in
            SVProgressHUD.dismiss()
            if let photos = response.result.value {
                self.flickrSearchResponse = photos
                self.photosCollectionView.reloadData()
            }
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flickrSearchResponse.photosResponse?.photo?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        if let photo = flickrSearchResponse.photosResponse?.photo?[indexPath.row], let imageUrl = photo.flickrImageURL() {
            cell.photoImageView.sd_setImageWithIndicator(with: imageUrl)
        }
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAt indexPath: IndexPath!) -> CGSize {
        return CGSize(width: 200, height: 200)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let flickrPhoto = flickrSearchResponse.photosResponse?.photo?[indexPath.row], let imageUrl = flickrPhoto.flickrImageURL("b") else {
            return
        }

        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCell
        let originImage = cell.photoImageView.image
        
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImageURL(imageUrl.absoluteString)
        photo.shouldCachePhotoURLImage = true
        images.append(photo)

        let browser = SKPhotoBrowser(originImage: originImage ?? UIImage(), photos: images, animatedFromView: cell)
        browser.initializePageIndex(indexPath.row)
        present(browser, animated: true, completion: {})
    }
    
}

extension ViewController: UISearchBarDelegate {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchString = searchBar.text else {
            return
        }
        
        searchFlickrPhoto(query: searchString)
    }
}

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
}
