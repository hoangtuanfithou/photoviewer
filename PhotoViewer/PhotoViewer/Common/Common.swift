//
//  Common.swift
//  PhotoViewer
//
//  Created by Nguyen Hoang Tuan on 3/10/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

extension UIImageView {
    func sd_setImageWithIndicator(with url: URL) {
        sd_setShowActivityIndicatorView(true)
        sd_setIndicatorStyle(.gray)
        sd_setImage(with: url)
    }
}
